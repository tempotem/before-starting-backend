# TempoTem - Teste Back-End

Olá caro desenvolvedor back-end, nesse teste analisaremos seu conhecimento básico e avançado sobre as tecnologias que utilizamos. Abaixo explicaremos tudo o que será necessário, lembrando que, "boas praticas" serão pontos positivos na sua avaliação.

## Descrição do teste
Essa avaliação consiste em testar seus conhecimentos de Javascript, Node, Sql e arquitetura de software baseada em microservices

O tempo ideal para realização da tarefa é de **3 dias**

* Prazo ideal: 3 dias

## O que você precisará fazer
Não aceitaremos pacote .zip, .rar ou qualquer outra extensão/arquivo que não seja exposta publicamente via GIT.

* Criar um microservico de authorization que deve conter:
 1 - endpoint para assinar o token JWT, este microservice deve consultar o segundo microservice e ver se o usuario existe, ele nao pode ter integracao com o banco de dados, totalmente statefull
 2 - decodar o token e ver se ele é valido

* Criar o segundo microservice de usuario
 1 - criar um middleware para decodar o token e ver se é valido e liberar a req para as rotas privadas.
 2 - endpoint publico para criar o usuario do qual deve conter as props name, phone, age.
 3 - endpoint privado para deletar o usuario.
 4 - endpoint privado para atualizar o usuario.

## Duvidas
ter_fabio.guelfi@tempotem.com.br

## Quais tecnologias deve usar
Atente-se a essas requisições, a falta de uma ou mais delas será motivo de penalização.

* Node
* Banco de dados postgres de preferencia, pode ser outro contato que contenha o dockercompose do banco.
* Axios para realizar as requisições
* ORM Sequelize ou typeorm.
* JWT OAuth2

## Critérios de aceite

* Projeto subindo.

Ao finalizar o teste encaminhar URL do repo para o e-mail: ter_fabio.guelfi@tempotem.com.br

